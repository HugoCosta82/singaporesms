using SingaporeSMS.Interfaces;
using GalaSoft.MvvmLight.Ioc;
using UIKit;

namespace SingaporeSMS.iOS.Code
{
   public static class RegisterServices
    {
        public static void Init(UIWindow Window)
        {
            
            if (!SimpleIoc.Default.IsRegistered<ILogger>())
                SimpleIoc.Default.Register<ILogger>(() => new Services.Logger());

            if (!SimpleIoc.Default.IsRegistered<INavigationService>())
                SimpleIoc.Default.Register<INavigationService>(() => new Services.NavigationService(Window));

            if (!SimpleIoc.Default.IsRegistered<IUiHelper>())
                SimpleIoc.Default.Register<IUiHelper>(() => new Services.UiHelper());

            if (!SimpleIoc.Default.IsRegistered<IConnectivityService>())
                SimpleIoc.Default.Register<IConnectivityService>(() => new Services.ConnectivityService());
			
            if (!SimpleIoc.Default.IsRegistered<ISMS>())
				SimpleIoc.Default.Register<ISMS>(() => new Services.SendSms());

            if (!SimpleIoc.Default.IsRegistered<IQRCode>())
                SimpleIoc.Default.Register<IQRCode>(() => new Services.QRCode());

        }
    }
}