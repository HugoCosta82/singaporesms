﻿using System;
using SingaporeSMS.ViewModels.Common;
using GalaSoft.MvvmLight.Threading;

namespace SingaporeSMS.iOS.Code
{
	public static class App
	{
		private static ViewModelLocator _locator;

		public static ViewModelLocator Locator
		{

			get
			{
				if (_locator == null)
				{
					_locator = new ViewModelLocator();
				}

				return _locator;

			}
		}
	}
}
