using Foundation;
using System;
using UIKit;

namespace SingaporeSMS.iOS
{
    public partial class TabController : UITabBarController
    {
		UIViewController tab1, tab2, tab3, tab4;

		public TabController()
		{

			var storyboard = UIStoryboard.FromName("Main", null);
			var mainController =(BalanceViewController) storyboard.InstantiateViewController("BalanceViewController");
            tab1 = mainController;
			tab1.Title = "Home";

			
			var sendController = (GenericOperationViewController)storyboard.InstantiateViewController("GenericOperationViewController");
            sendController.screenParam = SingaporeSMS.Code.Constants.SEND_MONEY_SCREEN;
            tab2 = sendController;
			tab2.Title = "Enviar";

			
			var receiveController = (ReceiveMoneyViewController) storyboard.InstantiateViewController("ReceiveMoneyViewController");
            tab3 = receiveController;
			tab3.Title = "Receber";

			
			var payController = (GenericOperationViewController) storyboard.InstantiateViewController("GenericOperationViewController");
            payController.screenParam = SingaporeSMS.Code.Constants.PAY_SCREEN;
			tab4 = payController;
			tab4.Title = "Pagar";


			var tabs = new UIViewController[] {
								tab1, tab2, tab3, tab4
						};

			ViewControllers = tabs;
		}


    }
}