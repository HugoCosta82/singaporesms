using Foundation;
using System;
using UIKit;
using SingaporeSMS.ViewModels;
using SingaporeSMS.iOS.Code;
using System.Collections.Generic;
using GalaSoft.MvvmLight.Helpers;

namespace SingaporeSMS.iOS
{
    public partial class BalanceViewController : UIViewController
    {
		private BalanceViewModel Vm => App.Locator.Balance;
		//private readonly List<Binding> _bindings = new List<Binding>();

        public BalanceViewController (IntPtr handle) : base (handle)
        {
        }


        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            SendButton.TouchUpInside+= (sender, e) => {};
            SendButton.SetCommand(
                nameof(SendButton.TouchUpInside),
                Vm.GoToSendMoney);

            ReceiveButton.TouchUpInside += (sender, e) => { };
            ReceiveButton.SetCommand(
                nameof(ReceiveButton.TouchUpInside),
                Vm.GoToReceiveMoney
               );

			PayButton.TouchUpInside += (sender, e) => { };
            PayButton.SetCommand(
                nameof(PayButton.TouchUpInside),
                Vm.GoToPay
            );
        }
    }
}