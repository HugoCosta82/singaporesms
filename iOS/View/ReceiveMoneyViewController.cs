	using Foundation;
	using System;
	using UIKit;
	using SingaporeSMS.ViewModels;
	using SingaporeSMS.iOS.Code;
	using System.Collections.Generic;
	using GalaSoft.MvvmLight.Helpers;
	using Microsoft.Practices.ServiceLocation;

	namespace SingaporeSMS.iOS
	{
	    public partial class ReceiveMoneyViewController : UIViewController
	    {
	        private ReceiveMoneyViewModel Vm => App.Locator.ReceiveMoney;
			private readonly List<Binding> _bindings = new List<Binding>();

	            public ReceiveMoneyViewController(IntPtr handle) : base(handle)
	            {
	            }

	        public override void ViewDidLoad()
	        {
	            base.ViewDidLoad();

			this.ValueTextField.ShouldReturn += (textField) =>
			{
				textField.ResignFirstResponder();
				return true;
			};

				_bindings.Add(
	                    this.SetBinding(
	                    () => Vm.Value,
	                    () => ValueTextField.Text,
	                    BindingMode.TwoWay
		        ).ObserveTargetEvent(nameof(ValueTextField.EditingChanged)));
				FinishOperationButton.SetCommand(
				 nameof(FinishOperationButton.TouchUpInside),
                Vm.ReceiveService);

	        }
	        
	    }
	}