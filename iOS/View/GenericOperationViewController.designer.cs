// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace SingaporeSMS.iOS
{
    [Register ("GenericOperationViewController")]
    partial class GenericOperationViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton FinishOperationButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel FirstLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField FirstTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton GoBackButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton QRButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel SecondLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField SecondTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITabBarItem Tab { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (FinishOperationButton != null) {
                FinishOperationButton.Dispose ();
                FinishOperationButton = null;
            }

            if (FirstLabel != null) {
                FirstLabel.Dispose ();
                FirstLabel = null;
            }

            if (FirstTextField != null) {
                FirstTextField.Dispose ();
                FirstTextField = null;
            }

            if (GoBackButton != null) {
                GoBackButton.Dispose ();
                GoBackButton = null;
            }

            if (QRButton != null) {
                QRButton.Dispose ();
                QRButton = null;
            }

            if (SecondLabel != null) {
                SecondLabel.Dispose ();
                SecondLabel = null;
            }

            if (SecondTextField != null) {
                SecondTextField.Dispose ();
                SecondTextField = null;
            }

            if (Tab != null) {
                Tab.Dispose ();
                Tab = null;
            }
        }
    }
}