	using Foundation;
	using System;
	using UIKit;
	using SingaporeSMS.ViewModels;
	using SingaporeSMS.iOS.Code;
	using System.Collections.Generic;
	using GalaSoft.MvvmLight.Helpers;
	using Microsoft.Practices.ServiceLocation;

	namespace SingaporeSMS.iOS
	{
	    public partial class GenericOperationViewController : UIViewController
	    {
	        private GenericOperationViewModel Vm => App.Locator.Generic;
	        private readonly List<Binding> _bindings = new List<Binding>();
            public  string screenParam = ""; 

	        public GenericOperationViewController(IntPtr handle) : base(handle)
	        {
	        }

	        public override void ViewDidLoad()
	        {
	            base.ViewDidLoad();
            Vm.DestinyNumber = "";
            Vm.Value = "";


    
			//workaround for dismiss keyboard
			var g = new UITapGestureRecognizer(() => View.EndEditing(true));
			g.CancelsTouchesInView = false; //for iOS5

			View.AddGestureRecognizer(g);

			this.FirstTextField.ShouldReturn += (textField) =>
			{
				textField.ResignFirstResponder();
				return true;
			};

            this.SecondTextField.ShouldReturn += (textField) =>
			{
				textField.ResignFirstResponder();
				return true;
			};

            //end 

            //FirstTextField.EditingChanged+= (sender, e) => {};
            //workaround for binding on real device
            FirstTextField.Text = "";
	            _bindings.Add(
	            this.SetBinding(
	                    () => Vm.DestinyNumber,
	                    () => FirstTextField.Text,
	           BindingMode.TwoWay
	                ).ObserveTargetEvent(nameof(FirstTextField.EditingChanged)));

			SecondTextField.EditingChanged += (sender, e) => { };
			_bindings.Add(
	                this.SetBinding(
	                () => Vm.Value,
                    () => SecondTextField.Text,
	                BindingMode.TwoWay
	            ).ObserveTargetEvent(nameof(SecondTextField.EditingChanged)));

			QRButton.TouchUpInside += (sender, e) => { };
	            QRButton.SetCommand(
	            nameof(QRButton.TouchUpInside),
	                Vm.GoToQRCode);

			GoBackButton.TouchUpInside += (sender, e) => { };
	            GoBackButton.SetCommand(
	            nameof(GoBackButton.TouchUpInside),
	                Vm.GoBack);

				Services.NavigationService n = (Services.NavigationService)
	                ServiceLocator.Current.GetInstance<SingaporeSMS.Interfaces.INavigationService>();

	            QRButton.SetTitle("Ler QR", new UIControlState());

				var param = (string)n.Nav.GetAndRemoveParameter(this);
                this.screenParam = param; 
	            if (this.screenParam.Equals(SingaporeSMS.Code.Constants.SEND_MONEY_SCREEN))
	            {
	                FirstLabel.Text = "Nr. destinatário";
	                SecondLabel.Text = "Valor";
		                FinishOperationButton.SetCommand(
		                 nameof(FinishOperationButton.TouchUpInside),
		                    Vm.ComposeSMS);
            } else if (this.screenParam.Equals(SingaporeSMS.Code.Constants.PAY_SCREEN))
	            {
					FirstLabel.Text = "Entidade";
					SecondLabel.Text = "Valor";

				FinishOperationButton.SetCommand(
				 nameof(FinishOperationButton.TouchUpInside),
					Vm.PayService);
                 }
	        
	        }



    }
	}