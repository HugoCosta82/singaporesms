// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace SingaporeSMS.iOS
{
    [Register ("ReceiveMoneyViewController")]
    partial class ReceiveMoneyViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton FinishOperationButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel ValueLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField ValueTextField { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (FinishOperationButton != null) {
                FinishOperationButton.Dispose ();
                FinishOperationButton = null;
            }

            if (ValueLabel != null) {
                ValueLabel.Dispose ();
                ValueLabel = null;
            }

            if (ValueTextField != null) {
                ValueTextField.Dispose ();
                ValueTextField = null;
            }
        }
    }
}