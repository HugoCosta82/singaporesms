﻿using System;
using Foundation;
using SingaporeSMS.Interfaces;
using UIKit;

namespace SingaporeSMS.iOS.Services
{
    public class SendSms:ISMS
    {
       
        void ISMS.SendSms(string sms)
        {
			var smsTo = NSUrl.FromString("sms:" + sms);
			if (UIApplication.SharedApplication.CanOpenUrl(smsTo))
			{
				UIApplication.SharedApplication.OpenUrl(smsTo);
			}
			else
			{
				// warn the user, or hide the button...
			}
        }
    }
}
