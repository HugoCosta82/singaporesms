﻿using System;
using System.Threading.Tasks;
using SingaporeSMS.Code;
using SingaporeSMS.Interfaces;
using UIKit;

namespace SingaporeSMS.iOS.Services
{
    public class NavigationService : INavigationService
    {
        public GalaSoft.MvvmLight.Views.NavigationService Nav;

		public NavigationService(UIWindow Window)
		{
			Nav = new GalaSoft.MvvmLight.Views.NavigationService();
			Nav.Initialize((UIKit.UINavigationController)Window.RootViewController);
            Nav.Configure("balance", nameof(BalanceViewController));
            Nav.Configure("genericOperation", nameof(GenericOperationViewController));
			Nav.Configure("receiveMoney", nameof(ReceiveMoneyViewController));
            Nav.Configure("qrView", nameof(QRViewController));

		}

        public void GoBack()
        {
            Nav.GoBack();
        }

        public Task NavigateToCamera()
        {
            throw new NotImplementedException();
        }

        public Task NavigateToGenericOperation(string page)
        {
            Nav.NavigateTo("genericOperation", page);
			return AsyncUtils.CompletedTaskAsync();
		}

        public void NavigateToHome()
        {
            throw new NotImplementedException();
        }

        public Task NavigateToQrCode()
        {
			Nav.NavigateTo("qrView", "");
			return AsyncUtils.CompletedTaskAsync();
        }

        public Task NavigateToReceiveMoney()
        {
            Nav.NavigateTo("receiveMoney", "");
            return AsyncUtils.CompletedTaskAsync();
        }

	}


}
