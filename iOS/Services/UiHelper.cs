﻿using System;
using System.Threading.Tasks;
using SingaporeSMS.Interfaces; 
using UIKit;


namespace SingaporeSMS.iOS.Services
{
    public class UiHelper : IUiHelper
    {
        public UiHelper()
        {
        }

        public async Task ShowAlert(string message)
        {
            GalaSoft.MvvmLight.Threading.DispatcherHelper.CheckBeginInvokeOnUI(()=> {
                openAlert("Alert", message); 
            });

            await SingaporeSMS.Code.AsyncUtils.CompletedTaskAsync(); 
        }

        public async Task ShowAlert(string title, string message)
        {

			GalaSoft.MvvmLight.Threading.DispatcherHelper.CheckBeginInvokeOnUI(() =>
			{
                openAlert(title, message); 
			});

			await SingaporeSMS.Code.AsyncUtils.CompletedTaskAsync();
		}

        private void openAlert (string title, string message){
			UIAlertView alert = new UIAlertView()
			{
                Title = title,
                Message = message
			};
			alert.AddButton("OK");
			alert.Show();
		}


    }
}
