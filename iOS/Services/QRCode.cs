﻿using System;
using System.Threading.Tasks;
using SingaporeSMS.Interfaces;

namespace SingaporeSMS.iOS.Services
{
    public class QRCode : IQRCode
    {
        public QRCode()
        {
        }

        public async Task<string> OpenQRCode()
        {
			var scanner = new ZXing.Mobile.MobileBarcodeScanner();
			var result = await scanner.Scan();

			if (result != null)
				Console.WriteLine("Scanned Barcode: " + result.Text);

            return result == null? "" : result.Text; 
        }

    }
}
