﻿using NUnit.Framework;
using SingaporeSMS.Code;

namespace SingaporeSMS.Tests
{
    [TestFixture()]
    public class Test
    {
        [Test]
        public void Pass()
        {
            Assert.True(true);
        }

        [Test]
        public void Fail()
        {
            Assert.False(false);
        }


        [Test]
        public void Money()
        {
            MyClass a = new MyClass();
            a.Money = 100;

            Assert.AreEqual(100, a.Money);
        }

        [Test]
        public void ConstantPAY_SCREEN(){
            var x = Constants.PAY_SCREEN; 

            StringAssert.AreEqualIgnoringCase(x, "PAY_SCREEN");
        }

		[Test]
		public void ConstantSEND_MONEY_SCREEN()
		{
            var x = Constants.SEND_MONEY_SCREEN;

            StringAssert.AreEqualIgnoringCase(x, "SEND_MONEY_SCREEN");
		}
    }
}
