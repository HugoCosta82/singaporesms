﻿﻿using System;

namespace SingaporeSMS.Interfaces
{
    public interface ILogger
    {
        void Log(Exception ex);
    }
}