﻿using System;
using System.Threading.Tasks;

namespace SingaporeSMS.Interfaces
{
    public interface IQRCode
    {
        Task<string> OpenQRCode();
    }
}
