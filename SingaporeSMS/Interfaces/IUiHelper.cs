﻿﻿using System.Threading.Tasks;

namespace SingaporeSMS.Interfaces
{
    public interface IUiHelper
    {
        Task ShowAlert(string message);
        Task ShowAlert(string title, string message);

    }
}
