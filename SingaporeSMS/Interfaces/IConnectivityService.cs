﻿﻿using System.Threading.Tasks;

namespace SingaporeSMS.Interfaces
{
    public interface IConnectivityService
    {
        bool IsConnected();

        Task<bool> IsRemoteReachable();

    }
}
