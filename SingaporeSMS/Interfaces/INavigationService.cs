﻿﻿using System.Threading.Tasks;

namespace SingaporeSMS.Interfaces
{
    public interface INavigationService
    {

		Task NavigateToReceiveMoney();

		Task NavigateToGenericOperation(string page);

		Task NavigateToCamera();

		Task NavigateToQrCode();

		void NavigateToHome();

		void GoBack();
      
    }
}