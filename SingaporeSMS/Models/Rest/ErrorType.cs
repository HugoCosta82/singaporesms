﻿namespace SingaporeSMS.Models.Rest
{

    public enum ErrorType { Http, Json, Other, FromServer };
}
