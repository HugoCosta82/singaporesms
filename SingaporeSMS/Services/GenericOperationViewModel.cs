﻿using System;
using GalaSoft.MvvmLight.Command;
using SingaporeSMS.Interfaces;
using SingaporeSMS.ViewModels.Common;



namespace SingaporeSMS.ViewModels
{
    public class GenericOperationViewModel: BaseViewModel
    {

        private readonly ISMS _sms;
        private readonly IQRCode _qrCode;

		public GenericOperationViewModel(IConnectivityService connectivityService
		, IUiHelper uiHelper
		, INavigationService navigationService
		, ILogger logger
        , ISMS sms
        , IQRCode qrCode
                                        ) : base(connectivityService, uiHelper, navigationService, logger)
        {
            _sms = sms;
            _qrCode = qrCode; 
		}


		private string _destinyNumber ;
		public string DestinyNumber
		{
			get => _destinyNumber;
			set
			{
				if (_destinyNumber == value) return;
				_destinyNumber = value;

				//IsFormOk = _searchQuery.Length > 3;
				NotifyPropertyChanged();

			}
		}

		private string _value;
		public string Value
		{
			get => _value;
			set
			{
				if (_value == value) return;
				_value = value;

				//IsFormOk = _searchQuery.Length > 3;
				NotifyPropertyChanged();

			}
		}


        private RelayCommand _composeSMS;
        public RelayCommand ComposeSMS
		{
			get
			{
                return _composeSMS ?? (_composeSMS = new RelayCommand(
			async () =>
			{

                try
                {
                        if (String.IsNullOrEmpty(this._value) || String.IsNullOrEmpty(this._destinyNumber))
                    {
                        await this.UiHelper.ShowAlert("Deve preencher todos os campos");
                    }
                    else
                    {

                        _sms.SendSms(this.DestinyNumber);

					}
                }
                catch (Exception ex)
                {

                    throw ex;
                }
                finally
                {

                }


			}));
			}

		}

		private RelayCommand _payService;
		public RelayCommand PayService
		{
			get
			{
				return _payService ?? (_payService = new RelayCommand(
			async () =>
			{

				try
				{
					if (!String.IsNullOrEmpty(this._value) && !String.IsNullOrEmpty(this._destinyNumber))
					{
						await this.UiHelper.ShowAlert("Serviço pago com sucesso");
					}
					else
					{
						await this.UiHelper.ShowAlert("Deve preencher todos os campos");
					}
				}
				catch (Exception ex)
				{

					throw ex;
				}
				finally
				{

				}

			}));
			}

		}


		private RelayCommand _goToQRCode;
		public RelayCommand GoToQRCode
		{
			get
			{
				return _goToQRCode ?? (_goToQRCode = new RelayCommand(
			async () =>
			{

				try
				{
                        this.DestinyNumber = await _qrCode.OpenQRCode(); 
	

				//Console.WriteLine("Scanned Barcode: " + result.Text);


				}
				catch (Exception ex)
				{

					throw ex;
				}
				finally
				{

				}
			}));
			}

		}


		
    }
}
