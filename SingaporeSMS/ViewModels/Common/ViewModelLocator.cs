﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using SingaporeSMS.ViewModels;

namespace SingaporeSMS.ViewModels.Common
{
    public class ViewModelLocator
    {
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);
            SimpleIoc.Default.Register<BalanceViewModel>();
			SimpleIoc.Default.Register<GenericOperationViewModel>();
            SimpleIoc.Default.Register<ReceiveMoneyViewModel>();
        }

        public BalanceViewModel Balance => ServiceLocator.Current.GetInstance<BalanceViewModel>();
        public GenericOperationViewModel Generic => ServiceLocator.Current.GetInstance<GenericOperationViewModel>();
		public ReceiveMoneyViewModel ReceiveMoney => ServiceLocator.Current.GetInstance<ReceiveMoneyViewModel>();

	}

}
