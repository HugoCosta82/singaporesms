﻿using System;
using GalaSoft.MvvmLight.Command;
using SingaporeSMS.Interfaces;
using SingaporeSMS.ViewModels;
using SingaporeSMS.ViewModels.Common;

namespace SingaporeSMS.ViewModels
{
    public class ReceiveMoneyViewModel: BaseViewModel
    {
		public ReceiveMoneyViewModel(IConnectivityService connectivityService
		, IUiHelper uiHelper
		, INavigationService navigationService
		, ILogger logger) : base(connectivityService, uiHelper, navigationService, logger)
        {
		}

		private string _value;
		public string Value
		{
			get => _value;
			set
			{
				if (_value == value) return;
				_value = value;

				//IsFormOk = _searchQuery.Length > 3;
				NotifyPropertyChanged();

			}
		}


		private RelayCommand _receiveService;
		public RelayCommand ReceiveService
		{
			get
			{
				return _receiveService ?? (_receiveService = new RelayCommand(
			async () =>
			{

				try
				{
					if (!String.IsNullOrEmpty(this._value))
					{
                            await this.NavigationService.NavigateToQrCode();
					}
					else
					{
						await this.UiHelper.ShowAlert("Deve preencher todos os campos");
					}
				}
				catch (Exception ex)
				{

					throw ex;
				}
				finally
				{

				}



			}));
			}

		}

	}

}
